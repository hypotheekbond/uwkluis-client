<?php
declare(strict_types = 1);

namespace UwKluis\Client\Consumer;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Exception;
use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Lcobucci\JWT\Token;
use Ramsey\Uuid\UuidFactoryInterface;
use Ramsey\Uuid\UuidInterface;
use UwKluis\Client\Client\UwkluisClientInterface;
use UwKluis\Client\Exception\ConsumerConnectionConflict;
use UwKluis\Client\Exception\ConsumerConnectionException;
use UwKluis\Client\Exception\OrganizationConnectionException;
use UwKluis\Client\Organization\Config;
use UwKluis\Enums\ConsumerConnection\Status;

/**
 * Class Connect
 */
final class Connect
{
    public function __construct(
        private readonly Config                 $config,
        private readonly UwkluisClientInterface $uwkluisClient,
        private readonly UuidFactoryInterface   $uuidFactory
    ) {
    }

    /**
     * @param Token $accessToken
     * @param string $email
     * @param string $phoneNumber
     * @param bool $disablePhoneNumberVerification
     *
     * @return Connection
     * @throws AssertionFailedException
     * @throws GuzzleException
     */
    public function inviteConsumer(
        Token $accessToken,
        string $email,
        string $phoneNumber,
        bool $manualVerification = false,
        bool $returnInviteLink = false,
        ?string $language = null,
        bool $businessAccount = false,
        ?string $eblinqxUuid = null,
        ?string $dossierName = null,
        ?string $initials = null,
        ?string $firstName = null,
        ?string $addition = null,
        ?string $lastName = null
    ): Connection {
        Assertion::email($email);

        try {
            $httpResponse = $this->uwkluisClient->request(
                RequestMethodInterface::METHOD_POST,
                $this->config->getApiHost() . '/consumer/invite',
                [
                    RequestOptions::FORM_PARAMS => [
                        'email'                             => $email,
                        'phone_number'                      => $phoneNumber,
                        'manual_verification'               => $manualVerification,
                        'language'                          => $language,
                        'return_invite_link'                => $returnInviteLink,
                        'business_account'                  => $businessAccount,
                        'eblinqx_uuid'                      => $eblinqxUuid,
                        'dossier_name'                      => $dossierName,
                        'initials'                          => $initials,
                        'first_name'                        => $firstName,
                        'addition'                          => $addition,
                        'last_name'                         => $lastName,
                    ],
                    RequestOptions::HEADERS     => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken->toString(),
                    ],
                ]
            );
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === StatusCodeInterface::STATUS_UNAUTHORIZED) {
                throw new OrganizationConnectionException('Organization connection failed', $e->getCode(), $e);
            }

            if ($e->getResponse()->getStatusCode() === StatusCodeInterface::STATUS_CONFLICT) {
                $response = json_decode($e->getResponse()->getBody()->getContents());

                $consumerUuid = $response->data->uwkluis_consumer_id;
                throw new ConsumerConnectionConflict(
                    $response->message,
                    $e->getCode(),
                    $e,
                    $consumerUuid ? new Connection($this->uuidFactory->fromString($consumerUuid)) : null
                );
            }

            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        } catch (Exception $e) {
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        }

        $data = json_decode($httpResponse->getBody()->getContents());

        return new Connection(
            $this->uuidFactory->fromString($data->uwkluis_consumer_id),
            null,
            null,
            $data->invite_link ?? null,
            null,
            $data->verification_code ?? null
        );
    }

    public function revokeInvite(Token $accessToken, string $consumerUuid): void
    {
        try {
            $this->uwkluisClient->request(
                RequestMethodInterface::METHOD_POST,
                $this->config->getApiHost() . '/consumer/revoke-invite?' . http_build_query(
                    [
                        'consumer_id' => $consumerUuid,
                    ]
                ),
                [
                    RequestOptions::HEADERS     => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken->toString(),
                    ],
                ]
            );
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === StatusCodeInterface::STATUS_UNAUTHORIZED) {
                throw new OrganizationConnectionException('Organization connection failed', $e->getCode(), $e);
            }
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        } catch (Exception $e) {
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        }
    }

    /**
     * @param Token $accessToken
     * @param UuidInterface $identifier
     * @param string $email
     * @param string $phoneNumber
     *
     * @return Connection
     * @throws AssertionFailedException
     * @throws GuzzleException
     */
    public function updateAndReinviteConsumer(
        Token $accessToken,
        UuidInterface $identifier,
        string $email,
        string $phoneNumber,
        bool $returnInviteLink = false,
        ?string $language = null,
        ?string $eblinqxUuid = null,
        bool $manualVerification = false,
        ?string $initials = null,
        ?string $firstName = null,
        ?string $addition = null,
        ?string $lastName = null
    ): Connection {
        Assertion::email($email);

        try {
            $httpResponse = $this->uwkluisClient->request(
                RequestMethodInterface::METHOD_POST,
                $this->config->getApiHost() . '/consumer/update-and-reinvite',
                [
                    RequestOptions::FORM_PARAMS => [
                        'email'               => $email,
                        'phone_number'        => $phoneNumber,
                        'manual_verification' => $manualVerification,
                        'consumer_identifier' => $identifier->toString(),
                        'language'            => $language,
                        'return_invite_link'  => $returnInviteLink,
                        'eblinqx_uuid'        => $eblinqxUuid,
                        'initials'            => $initials,
                        'first_name'          => $firstName,
                        'addition'            => $addition,
                        'last_name'           => $lastName,
                    ],
                    RequestOptions::HEADERS     => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken->toString(),
                    ],
                ]
            );
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === StatusCodeInterface::STATUS_UNAUTHORIZED) {
                throw new OrganizationConnectionException('Organization connection failed', $e->getCode(), $e);
            }
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        } catch (Exception $e) {
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        }

        $data = json_decode($httpResponse->getBody()->getContents());

        return new Connection(
            $this->uuidFactory->fromString($data->uwkluis_consumer_id),
            null,
            null,
            $data->invite_link ?? null,
            null,
            $data->verification_code ?? null
        );
    }

    /**
     * @param Token         $accessToken
     * @param UuidInterface $identifier
     *
     * @return Connection
     * @throws GuzzleException
     */
    public function getConnectionStatus(
        Token $accessToken,
        UuidInterface $identifier
    ): Connection {
        try {
            $httpResponse = $this->uwkluisClient->request(
                RequestMethodInterface::METHOD_GET,
                $this->config->getApiHost() . '/consumer/get-connection-status?' . http_build_query(
                    [
                        'consumer_identifier' => $identifier->toString(),
                    ]
                ),
                [
                    RequestOptions::HEADERS => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken->toString(),
                    ],
                ]
            );
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === StatusCodeInterface::STATUS_UNAUTHORIZED) {
                throw new OrganizationConnectionException('Organization connection failed', $e->getCode(), $e);
            }
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        } catch (Exception $e) {
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        }

        $connection = json_decode($httpResponse->getBody()->getContents());

        return new Connection(
            $this->uuidFactory->fromString($connection->uwkluis_consumer_id),
            new Status($connection->status),
            $connection->granted_scopes ? explode(' ', $connection->granted_scopes) : null,
            null,
            $connection->eblinqx_uuid ?? null
        );
    }

    /**
     * @param UuidInterface $uuid
     *
     * @return string
     */
    public function getOrganizationConsumerDossierUrl(UuidInterface $uuid): string
    {
        return $this->config->getOrganizationHost() . '/consumers/' . $uuid->toString();
    }

    /**
     * @return string
     */
    public function getOrganizationConsumersUrl(): string
    {
        return $this->config->getOrganizationHost() . '/consumers/';
    }

    /**
     * @param Token $accessToken
     *
     * @param UuidInterface $identifier
     *
     * @throws GuzzleException
     */

    public function disconnect(
        Token $accessToken,
        UuidInterface $identifier
    ): void
    {
        try {
            $this->uwkluisClient->request(
                RequestMethodInterface::METHOD_POST,
                $this->config->getApiHost() . '/consumer/disconnect?' . http_build_query(
                    [
                        'consumer_id' => $identifier->toString(),
                    ]
                ),
                [
                    RequestOptions::HEADERS => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken->toString(),
                    ],
                ]
            );
        } catch (Exception $e) {
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        }
    }

    /**
     * @throws GuzzleException
     */
    public function getEmail(
        Token $accessToken,
        UuidInterface $identifier
    ): Email {
        try {
            $httpResponse = $this->uwkluisClient->request(
                RequestMethodInterface::METHOD_GET,
                $this->config->getApiHost() . '/consumer/email?' . http_build_query(
                    ['consumer_identifier' => $identifier->toString()]
                ),
                [
                    RequestOptions::HEADERS => [
                        'Accept'        => 'application/json',
                        'Authorization' => 'Bearer ' . $accessToken->toString(),
                    ],
                ]
            );
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === StatusCodeInterface::STATUS_UNAUTHORIZED) {
                throw new OrganizationConnectionException('Organization connection failed', $e->getCode(), $e);
            }
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        } catch (Exception $e) {
            throw new ConsumerConnectionException('Consumer connection failed', $e->getCode(), $e);
        }

        $responseContent = json_decode($httpResponse->getBody()->getContents());

        return new Email($responseContent->email);
    }
}

<?php
declare(strict_types = 1);

namespace UwKluis\Client\Exception;

/**
 * Class ConsumerConnectionException
 */
class ConsumerConnectionException extends \RuntimeException
{
}

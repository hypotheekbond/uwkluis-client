<?php
declare(strict_types = 1);

namespace UwKluis\Client\Exception;

/**
 * Class OrganizationConnectionException
 */
class OrganizationConnectionException extends \RuntimeException
{
}

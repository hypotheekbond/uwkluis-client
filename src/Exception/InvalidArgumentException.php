<?php
declare(strict_types=1);

namespace UwKluis\Client\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}
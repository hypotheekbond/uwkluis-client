<?php
declare(strict_types = 1);

namespace UwKluis\Client\Exception;

/**
 * Class RefreshTokenInvalidException
 */
final class InvalidScopesException extends InvalidRequestException
{
}

<?php
declare(strict_types = 1);

namespace UwKluis\Client\Exception;

/**
 * Class InvalidRequestException
 */
class InvalidRequestException extends \RuntimeException
{
}

<?php
declare(strict_types=1);

namespace UwKluis\Client\Client;

use GuzzleHttp\Client;

class UwkluisClient extends Client implements UwkluisClientInterface
{
}
